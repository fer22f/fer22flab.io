---
title: "Epoch-Gregorian Algorithm: V8's Implementation"
authors: ["fer22f"]
date: 2020-01-20
slug: epoch-gregorian-v8
---

We will be taking a look at different implementations of the algorithm I call "epoch-gregorian", which is simply the algoritm that you use to convert from epoch to a gregorian date, and vice-versa.

Today it is V8's implementation time. It can be found (as of the today) in [/src/date/date.cc](https://github.com/v8/v8/blob/8c8bd658c6f3a765a73ac49e57b7b138a67db635/src/date/date.cc). The functions we want to take a look at today are `DateCache::YearMonthDayFromDays` and `DateCache::DaysFromYearMonth`.

So let's start by checking out `YearMonthDayFromDays`, which has the following prototype:

```cpp
void DateCache::YearMonthDayFromDays(int days, int* year, int* month,
                                     int* day) {
```

We need to define precisely what these arguments mean. Seems simple enough. It's a function with takes `days` as input and `year`, `month`, `day` pointers which will be the output. So what does the input `days` mean?

* `days = 0` is 0 days from 1970-01-01, so 1970-01-01.
* `days = 364` is 364 days from 1970-01-01, so 1970-12-31.
* `days = 365` is 365 days from 1970-01-01, so 1971-01-01 (1970 not a leap year).

And now for the output, which is equivalent to JavaScript's `Date` type:

* `year` is the year indexed as per ISO 8601, so year 1 is 1 CE, and year 0 is 1 BCE.
* `month` is the month indexed from 0 to 11 inclusive. 0 is January, 11 is December.
* `day` is the day indexed from 1 to 28, 29, 30 or 31 depending on the month.

Now that that is somewhat clear, let's look at the body of the function.

```cpp
  if (ymd_valid_) {
    // Check conservatively if the given 'days' has
    // the same year and month as the cached 'days'.
    int new_day = ymd_day_ + (days - ymd_days_);
    if (new_day >= 1 && new_day <= 28) {
      ymd_day_ = new_day;
      ymd_days_ = days;
      *year = ymd_year_;
      *month = ymd_month_;
      *day = new_day;
      return;
    }
  }
```

First, since we have a cache, we check if we already have the correct date cached. A valid cache is given by `ymd_valid_`, which stores the cached input `days` as `ymd_days_` and the cached outputs `day`, `month`, `year` as `ymd_day_`, `ymd_month_` and `ymd_year_` respectively.

We use the cache if the difference of the cached `ymd_days_` and `days` results in the same year and month, and we fail early with 29, 30, 31 problems by conservatively bailing out after 28.

So let's see the algorithm in action!

```cpp
  int save_days = days;
```

First we save the original `days` value for future checks and also so we can cached it into `ymd_days_` later.

And then we start with the modulos. But before we get into the algorithm itself, we have to understand the constants at the top of the file.

```cpp
static const int kDaysIn4Years = 4 * 365 + 1;
```

First, we have a constant that is the number of days in a 4 year period, where one year is a leap year, but keep in mind this is not true for all four year periods. The period comprised of {{<katex inline>}}[1900,1904){{</katex>}} (all days of 1900, 1901, 1902 and 1903) for example, is {{<katex inline>}}4\cdot365{{</katex>}} days due to 1900 not being a leap year.

```cpp
static const int kDaysIn100Years = 25 * kDaysIn4Years - 1;
```

Then we have a constant which is the number of days in a 100 year period, where one year is not a leap year, but keep in mind this is also not true for all hundred year periods. For example, {{<katex inline>}}[2000,2100){{</katex>}} has exactly {{<katex inline>}}25\cdot(4\cdot365 + 1){{</katex>}} days, since 2000 is a leap year.

```cpp
static const int kDaysIn400Years = 4 * kDaysIn100Years + 1;
```

Now we have an universal, which is the number of days in a 400 year period. This is always true for **any** 400 year period in the Gregorian calendar, since it cycles every 400 years.

```cpp
static const int kDays1970to2000 = 30 * 365 + 7;
```

Then we can look at what is useful to us in converting from and to epoch: Days relative to 1970 (our baseline). The period of {{<katex inline>}}[1970, 2000){{</katex>}} is composed of 30 years, of which 1972, 1976, 1980, 1984, 1988, 1992 and 1996 are leap years which amounts to having {{<katex inline>}}(2000 - 1972) / 4 = 7{{</katex>}} more days.

```cpp
static const int kYearsOffset = 400000;
```

One arbitrary thing. We use the year 400&thinsp;000 as our base. From the [language spec, section 15.9.1.1](https://ecma-international.org/ecma-262/5.1/#sec-15.9.1.1), the minimum and maximum `Date`s vary between {{<katex inline>}}-100&thinsp;000&thinsp;000{{</katex>}} and {{<katex inline>}}100&thinsp;000&thinsp;000{{</katex>}} days, so if we want to always compute days in positive numbers, we need to shift it by at least 100&thinsp;000&thinsp;000 days.

The arbitrary choice of 400&thinsp;000 years gives us a shift of 146&thinsp;097&thinsp;000 days, but we could also use 300&thinsp;000, which would give us a shift of 109&thinsp;572&thinsp;750 days. In the end of day, it doesn't matter, as long as our integers don't overflow.

```cpp
static const int kDaysOffset =
    1000 * kDaysIn400Years + 5 * kDaysIn400Years - kDays1970to2000;
```

And then we have our most useful constant, which is indeed quite weird. It's the amount of days from year 0 (the first day of 1 BCE) to year 400&thinsp;000, minus the amount of days in {{<katex inline>}}[1970, 2000){{</katex>}}.

The fact that we subtract the amount of days in {{<katex inline>}}[1970,2000){{</katex>}} is only symbolic, because that amount of days is the same as in {{<katex inline>}}[401&thinsp;970,402&thinsp;000){{</katex>}}. So, this constant could also be described as the amount of days in {{<katex inline>}}[0, 401&thinsp;970){{</katex>}}, or perhaps the amount of days in {{<katex inline>}}([0, 1970) + [0, 400&thinsp;000)){{</katex>}}, like follows:

```cpp
static const int kDaysOffset =
  ((2000 / 400) * kDaysIn400Years - kDays1970to2000) +
  (kYearsOffset / 400) * kDaysIn400Years;
```

Regardless of its definition, we will use this to adjust the given day to the 400 year cycle. Let's go back to the function.

```cpp
  days += kDaysOffset;
```

Here we see the first bit of action using our previously defined constants. We shift the days by `kDaysOffset` (so that the number of days is always positive afterwards).

```cpp
  *year = 400 * (days / kDaysIn400Years) - kYearsOffset;
  days %= kDaysIn400Years;
```

Now since we are aligned to the 400 year grid, we can get the position in that 400 year grid and set the year to this value. We also modulo our days, we won't be dealing with time intervals bigger than 400 years now.

```cpp
  DCHECK_EQ(save_days, DaysFromYearMonth(*year, 0) + days);
```

Here we have a sanity debug check to make sure that the days that are left plus the year that we computed correspond to what we saved.

```cpp
  days--;
```

Now this corresponds to the extra day in the 400th leap year, which we remove. To understand it, you can think of passing the number of days in {{<katex inline>}}[1970,2000){{</katex>}} to this function (2020-01-01), 10&thinsp;957 days. After all the calculations above, `days = 0`.

```cpp
  int yd1 = days / kDaysIn100Years;
  days %= kDaysIn100Years;
  *year += 100 * yd1;

  days++;
  int yd2 = days / kDaysIn4Years;
  days %= kDaysIn4Years;
  *year += 4 * yd2;

  days--;
  int yd3 = days / 365;
  days %= 365;
  *year += yd3;

  bool is_leap = (!yd1 || yd2) && !yd3;

  DCHECK_GE(days, -1);
  DCHECK(is_leap || (days >= 0));
  DCHECK((days < 365) || (is_leap && (days < 366)));
  DCHECK(is_leap == ((*year % 4 == 0) && (*year % 100 || (*year % 400 == 0))));
  DCHECK(is_leap || ((DaysFromYearMonth(*year, 0) + days) == save_days));
  DCHECK(!is_leap || ((DaysFromYearMonth(*year, 0) + days + 1) == save_days));

  days += is_leap;

  // Check if the date is after February.
  if (days >= 31 + 28 + (is_leap ? 1 : 0)) {
    days -= 31 + 28 + (is_leap ? 1 : 0);
    // Find the date starting from March.
    for (int i = 2; i < 12; i++) {
      if (days < kDaysInMonths[i]) {
        *month = i;
        *day = days + 1;
        break;
      }
      days -= kDaysInMonths[i];
    }
  } else {
    // Check January and February.
    if (days < 31) {
      *month = 0;
      *day = days + 1;
    } else {
      *month = 1;
      *day = days - 31 + 1;
    }
  }
  DCHECK(DaysFromYearMonth(*year, *month) + *day - 1 == save_days);
  ymd_valid_ = true;
  ymd_year_ = *year;
  ymd_month_ = *month;
  ymd_day_ = *day;
  ymd_days_ = save_days;
}
```
