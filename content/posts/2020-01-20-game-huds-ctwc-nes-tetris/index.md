---
title: "Game HUDs: CTWC NES Tetris"
authors: ["fer22f"]
date: 2020-01-20
slug: game-huds-ctwc-nes-tetris
---

So today I start the "Game HUDs" series, we will be taking a look at different
heads-up displays present in many different gaming contexts to understand them
and to perhaps draw some inspiration.

Today we will take a look at one that piqued my interest. It's the one used
at the Classic Tetris World Championship, such as in the [Joseph vs. Koryan final
battle of 2019](https://www.youtube.com/watch?v=nfo8hmIcoDQ):

![Screenshot from the video which shows two players in tetris action with overlays in the middle separating both](full-hud.jpg)

There's plenty to unpack here from this beautiful 8-bit UI. We can see a lot of
inspiration from the original tetris game because all of the lettering is using
a similar font and a lot of it is copied straight from the game, so let's
take a look at each element.

## The main match

This is what overlay elements see for the entirety of the match. They are
always present, and show the current status of each player's game.

### Scoreboard

![Two square boxes, each written "SCORE" at the top, "274360" and "332500" respectively in the second line, "-058140" in red and "+058140" in green respectively in the third line](score.png)

The scoreboard is an essential part of the competition, because it's what decides
who wins. That's why we have both players points being shown, and right below that,
how much each player is behind or ahead of the other.

So with this symmetrical design, you can quickly see who is winning by checking
whether the player's score is green, or has a + prefix. And the currently losing
player has instead a red score, and a - prefix to signify it's behind by that
many points.

This scoreboard is a bit different from the original game because the original
maxes out at 999999[^1]. This counter however has one extra space which can be used
to keep going up to 9999999, and that has been proven to be enough for tournaments.

[^1]: As far as I know, The competition does use the original unmodified NES cartridge of the game -- although in some games, they use some Game Genie-style trickery to set specific RNG seeds -- which means that the score players see at their console will not match the one used to compete (the one being shown here) once they max out.

### Line counter

![A square box written "LINE" at the top and "072" at the bottom](line.png#center)

This one is 100% copied straight from the original game. It simply shows how
many lines have been cleared so far. This is important because the game speed
is based on the level, which goes up when a certain threshold of lines is met.

### Next tetromino

![A square box with a four block square tetromino](next-tetromino.png#center)

Also copied from the game, it shows the next tetromino piece that can be played
after the current one is placed. This aids the player in preparing the next
sequence of inputs and also on what to do with the current piece.

### Game board

![A square box containing a tetris game, which is composed of lots of tetrominos stacked on each other](game-board.png#center)

This is the playing area where the action of the game happens. Being colorful
helps we segment the tetrominos which slowly get truncated when their individual
blocks are used to clear a line.

When a player advances to the next level, however, the playing field gets a
new palette of colors, which are used by players to identify that the game event
happened and what level they are now playing. The color palettes include yellow,
green and pink and are part of the pace of tournament gameplay.

### Level counter

![A square box written "LV" at the top and "18" at the bottom](level.png#center)

Another one straight from the game, this identifies the level itself,
which mandates the speed of the game, the color palette of the board and
the score multiplier.

The competition mandates that the player start at a specific level with a specific
game seed, so that both games have the same order and set of tetrominos, and also
so the competition ends quicker (going from level 1 to 18 is quite the journey).

### Burn counter

![A square box written "BRN" at the top and "008" at the bottom](burn.png#center)

This one is a new invention and helps everyone be aware of how the game is going.
It counts burns since the last tetris, but that doesn't make much sense to any
casual tetris player, so let's roll the explanation.

To win a game of tetris, your goal should be to make as many tetrises as possible.
And what *is* a tetris? It's a fancy name for *clearing four lines at once*, and
you can only do that by using a line tetramino, which is four blocks in sequence.

However, we mess up. It's almost impossible to always mantain the board in
such a way that we only clear four lines at once and only that. So when we
don't, it's said that we burned those lines and the burn counter goes up for
each line that we burn.

Essentially, it's called that because a bunch of blocks were wasted
on a non-tetris clear, and tetris clears are valued a *lot* more. In level 0,
a tetris is worth 1&thinsp;200 points, while triples are worth 300, doubles 100
and singles only 40.

You can see the counter in action here at the left:

<video controls muted playsinline src="burn.webm"></video>

### Tetris rate

![A square box written "TRT" at the top and "77%" at the bottom](rate.png#center)

As explained before, tetrises are very important. As such, right after you
complete a tetris in your game, this indicator shows up in the place of the burn
counter. It shows how good you are at avoiding burns.

To calculate the tetris rate, the equation is simple:

{{< katex >}}
  \frac{\text{L}_{\text{tetris}} - \text{L}_{\text{burned}}}{\text{L}}
{{< /katex >}}

That is, the total of tetris lines (which is always a multiple of four) minus
the total of lines burned, and all of that divided by the total of lines
cleared (which is the sum of those two, too).

So, in general, having a higher tetris rate than your oponent means you are
winning, but not always. You can still lose while having a higher tetris rate
because the level multiples your clear points. That means that if at the higher
levels you end up doing a lot more tetrises than your oponent, you can win.

This counter is replaced by a reset burn counter once you burn at least one line.
This way, you can see what the last play was: If the tetris rate is showing, it
was a tetris, if it isn't, some lines were burned.

### Tetraminos since last straight

![A square box with a red straight tetramino at the top and "014" in red at the bottom](straight.png#center)

The NES version of tetris is perhaps a bit unfair. This is because the primary
engine of the game, the Random Generator, is completely random. That would be
the opposite of unfair, right? Well, it could perhaps be classified as unfair
because it leads to **drought**.

Because tetrises are the most valuable move of the game, the straight pieces
are very very valuable. Usually you don't want to miss the chance of using
them for completing a tetris. However, because the random algorithm is completely
random, you may end up waiting for them for too long while your board clutters.

Newer games fix this by using a bag system, which means that you never go too
much time without any specific piece. As we don't have that here, it's nice to
see how bad the situation actually is, so that we can blame the game instead of
our abilities.

This counter shows up, with the straight tetramino pulsating in red whenever
all the 13 last pieces played weren't straight tetraminos, a drought. It keeps
going up after each piece is spawned, until a straight tetramino is finally
in the game.

It should be noted that when both games are set to the same RNG seed,
droughts will happen in both sides at similar times. If it happened to your
oponent, it will happen, sooner or later to you, it just means that you're in
slower pace than that of your enemy.

### Controller peek

![A square box showing the hands of a player using the NES controller](peek.jpg#center)

As a curiosity, or to make the competition more legitimate, a peek of the
player's hand in the controller is also shown. With this, you can verify if
the player is using hypertapping[^2] or not, for example.

[^2]: Check out [What is DAS and Hyper Tapping in Tetris by Simon](https://simon.lc/what-is-das-and-hyper-tapping-in-tetris)!

### Name and matches won

![A square box written "KORYAN" at the top, and three hearts at the bottom, one red and the others gray](matches.png#center)

Tournaments usually have multiple matches to declare a winner. In this case,
it's a best of 5, so players have to win at least 3 matches to win the battle.
Inspired by Zelda, and similar games, the wins show up as filled hearts below
the player's name.

## The end of the match

![Two square boxes with a bunch of stats and two colorful graphs](end.png)

When the match ends, we are presented a bunch of cryptic stats. Let's take a
look at each one:

| Screen Name | Description       |
|-------------|-------------------|
| TRT         | Final tetris rate. |
| I PIECE     | Shows how many straight pieces were spawned, `--` if the oponent is playing. |
| PPL        | Points per line, simply divide the amount of points by the amount of lines. |
| TRNS     | Total number of points after the first clear in level 19 (yellow palette), `000000` otherwise. |
| D. TOTAL | Amount of droughts (13+ last pieces not straight). |
| D. AVG  | Average streak of no straight tetraminos (drought). |
| D. MAX  | Maximum streak of no straight tetraminos (drought). |

And we also have two colorful graphs. The first one is a bar graph that shows
the distribution of points by segments of 10 lines. The y axis shows the amount
of points, and each vertical bar represents 10 lines. When a transition happens,
the bar gets the color of that level palette, like the first 10 lines of level
20, you can see the blue color, which is dominant color of the level.

The second graph shows how tight the competition was. The x axis scales with
time and the y axis shows how big of a difference existed between the players,
green and above the middle when the player was winning, and red and below the
middle when the player was losing.

And by looking at this last graph you would think that the first player lost,
but they actually won at the last second, as the score clearly shows. This is because
Koryan lost at the 19th level (you can see the last line of the previous graph
is yellow), and Joseph kept going, winning by getting their points back in the
difficult 20th and 21st levels.

## The takeaway

It seems like these overlays have been present since 2017, and the end of match
statistics since 2018. They seem like good indicators for players to look up to
and match, but the collapsed names can be a bit confusing for newcomers
(but probably the only thing that fits the 8-bit look, if we avoid scrolling
stats).

Perhaps what could be improved is showing if the match is seeded or not, how
the first graph is separated in the x axis (by tenths), and maybe try showing
the distribution of the other types of clears and hard drop stats
(which are basically invisible currently, you'll see some people using it,
and some don't).

But overall the overlays are absolutely great. Pretty, colorful, and very
thematic, simply beautiful and fitting! The community is perhaps one of the
most welcoming of beginners and one of the nicest in the speedrunning world,
it's as if we are watching just a couple of friends having fun, not really a
competition. Keep up the great work!
